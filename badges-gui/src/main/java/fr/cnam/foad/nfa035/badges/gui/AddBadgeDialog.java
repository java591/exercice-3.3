package fr.cnam.foad.nfa035.badges.gui;

import fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import org.jdesktop.swingx.JXDatePicker;

import javax.swing.*;
import java.awt.event.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;


public class AddBadgeDialog extends JDialog implements PropertyChangeListener {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField codeSerie;
    private JXDatePicker dateDebut;
    private JXDatePicker dateFin;
    private JFileChooser fileChooser;

    private BadgeWalletGUI gui;

    private DigitalBadge badge;

    private DirectAccessBadgeWalletDAO dao;
/**
 * constructeur
 */
    public AddBadgeDialog() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    /**
     * action du bouton OK
     */
    private void onOK() {
        // add your code here
        dispose();
    }

    /**
     * action bouton cancel
     */
    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    /**
     * retourne un booléen vrai si les champs du dialog sont remplis
     * @return
     */
    private boolean validateForm(){
        boolean validate =false;
        if(codeSerie.getText().trim().length()>0&&dateDebut.getDate()!=null&&dateFin.getDate()!=null&&fileChooser.getSelectedFile()!=null)
            validate=true;
        return validate;
    }

    /**
     * main
     * @param args
     */
    public static void main(String[] args) {
        AddBadgeDialog dialog = new AddBadgeDialog();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }

    /**
     * change la disponibilité du bouton OK en fonction des champs à remplir
     * @param evt A PropertyChangeEvent object describing the event source
     *          and the property that has changed.
     */
    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (validateForm()){
            this.buttonOK.setEnabled(true);
        }
        else{
            this.buttonOK.setEnabled(false);
        }
    }

}
